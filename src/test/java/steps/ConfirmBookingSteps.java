package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.httpclient.HttpException;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ConfirmBookingSteps {

	private static final StepsTestContextAndUtils stepsUtils =  StepsTestContextAndUtils.getInstance();

	@When("I confirm booking with resource {string} with parameters:")
	public void i_confirm_booking_with_parameters(String resource, Map<String, String> data)
			throws HttpException, IOException {
		stepsUtils.executePost(resource, data, StepsTestContextAndUtils.USERGUEST_HOST);

	}

	@When("I confirm booking with resource {string} with no parameters:")
	public void i_confirm_booking_with_Null_parameters(String resource, Map<String, String> data)
			throws HttpException, IOException {
		stepsUtils.executePost(resource, data,  StepsTestContextAndUtils.USERGUEST_HOST);

	}

	@Then("the response is not empty")
	public void the_response_is_different_is_not_equal() {
		assertNotNull(stepsUtils.responseBody);
	}


	@Then("the response is empty")
	public void the_response_is_empty() {
		assertEquals(stepsUtils.responseBody.length, 0);
	}
	
	@Then("the response is bizare")
	public void the_response_is_bizare() {
	    System.out.println("oooooooooooooooooooookkkkk");
	}


}
