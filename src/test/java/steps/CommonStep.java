package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * Class responsible for launching commons features Example: Authentication
 * Feature or Verifying HTTP code return
 */

public class CommonStep {
	private static final StepsTestContextAndUtils stepsUtils = StepsTestContextAndUtils.getInstance();
	private static String response_body;

	@When("I login with resource {string} with parameters:")
	public void i_login_with_resource_with_parameters(String resource, Map<String, String> data)
			throws JsonProcessingException, UnsupportedEncodingException, HttpException, IOException {
		response_body = stepsUtils.executePost(resource, data, StepsTestContextAndUtils.USERGUEST_BACK_OFFICE_HOST);
	}

	@Then("the response start with {string}")
	public void the_response_start_with(String string) throws JsonMappingException, JsonProcessingException {
		assertNotNull(StringUtils.startsWith(stepsUtils.getJsonValue(response_body, "id_token"), string));
	}

	@Then("Http code is {int}")
	public void http_code_is(Integer code) {
		assertEquals(stepsUtils.httpCode, code);

	}

	@Then("save the token for testing purposes")
	public void save_the_token_for_testing_purposes() throws JsonMappingException, JsonProcessingException {
	stepsUtils.admni_token=stepsUtils.getJsonValue(response_body, "id_token");
	}

}
