package steps;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

/**
 * Class responsible for launching all the functionals Cucumber Tests
 */

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/features", glue= {"steps"},strict = true,
monochrome=true, plugin= {"pretty","html:target/htmlReport"})
public class AllFeaturesTestRunner {

}
