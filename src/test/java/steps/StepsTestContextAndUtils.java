package steps;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Class responsible for encaplsulating the necessery needed utils for
 * calling(Get,POst, PUT, Tokens ...) business API for the purposes of
 * functional testing
 */

public class StepsTestContextAndUtils {

	private static final String PROPERTIES_FILE_NAME_EXTENSION = ".properties";
	private static final String PROPERTIES_FILE_NAME_PREFIX = "test-";
	private static final String TARGET_ENVIRONMENT_SUFFIX = "targetedenvironment";
	protected static final String USERGUEST_BACK_OFFICE_HOST = "userguest_back_office_host";
	protected static final String USERGUEST_HOST = "userguest_host";
	protected static final int HTTP_200 = 200;
	protected static final String UTF_8 = "UTF-8";
	protected static final String APPLICATION_JSON = "application/json";
	protected Integer httpCode;
	protected final HttpClient httpClient = new HttpClient();
	protected byte[] responseBody;
	protected ObjectMapper objectMapper = new ObjectMapper();
	protected String admni_token = "";

	private StepsTestContextAndUtils() {
	}

	private static class SingletonHelper {
		private static final StepsTestContextAndUtils INSTANCE = new StepsTestContextAndUtils();
	}

	public static StepsTestContextAndUtils getInstance() {
		return SingletonHelper.INSTANCE;
	}

	protected String getValueFromProperties(String key) throws IOException {

		Properties prop = new Properties();
		InputStream fis;
		String vmarg = Optional.ofNullable(System.getProperty(TARGET_ENVIRONMENT_SUFFIX))
				.orElseThrow(() -> new IllegalArgumentException("Vmarg(DEV,RCC or PRD) not provided at runtime"));
		
		StringBuilder propertiesFileName = new StringBuilder().append(PROPERTIES_FILE_NAME_PREFIX).append(vmarg)
				.append(PROPERTIES_FILE_NAME_EXTENSION);

		fis = getClass().getClassLoader().getResourceAsStream(propertiesFileName.toString());
		prop.load(fis);
		return prop.getProperty(key);
	}

	protected String executePost(String request, Map<String, String> data, String baseUrl)
			throws JsonProcessingException, UnsupportedEncodingException, IOException, HttpException {
		String jacksonData = objectMapper.writeValueAsString(data);
		StringRequestEntity requestEntity = new StringRequestEntity(jacksonData, StepsTestContextAndUtils.APPLICATION_JSON,
				StepsTestContextAndUtils.UTF_8);

		PostMethod postMethod = new PostMethod(getValueFromProperties(baseUrl) + request);
		postMethod.setRequestEntity(requestEntity);

		httpCode = httpClient.executeMethod(postMethod);
		responseBody = postMethod.getResponseBody();
		return new String(responseBody);
	}

	protected void executePut(String request, Map<String, String> data, String baseUrl)
			throws JsonProcessingException, UnsupportedEncodingException, IOException, HttpException {
		String jacksonData = objectMapper.writeValueAsString(data);
		StringRequestEntity requestEntity = new StringRequestEntity(jacksonData, StepsTestContextAndUtils.APPLICATION_JSON,
				StepsTestContextAndUtils.UTF_8);

		PutMethod putMethod = new PutMethod(getValueFromProperties(baseUrl) + request);
		putMethod.setRequestEntity(requestEntity);

		httpCode = httpClient.executeMethod(putMethod);
		responseBody = putMethod.getResponseBody();
	}

	protected void executeDelete(String request, Map<String, String> data, String baseUrl)
			throws JsonProcessingException, UnsupportedEncodingException, IOException, HttpException {

		DeleteMethod postMethod = new DeleteMethod(getValueFromProperties(baseUrl) + request);

		httpCode = httpClient.executeMethod(postMethod);
		responseBody = postMethod.getResponseBody();
	}

	public String getJsonValue(String json, String key) throws JsonMappingException, JsonProcessingException {
		final JsonNode node = objectMapper.readTree(json);
		return node.get(key).asText();

	}

}