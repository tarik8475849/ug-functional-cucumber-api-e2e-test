  Feature: Commons scenarios
    
      Scenario:  Login Admin Test

  When I login with resource '/api/authenticate' with parameters:

    | username | admin   |
    | password   |  test  |
    | rememberMe  |  false  |
    
  Then Http code is 200
  Then the response start with 'ey'
  Then save the token for testing purposes