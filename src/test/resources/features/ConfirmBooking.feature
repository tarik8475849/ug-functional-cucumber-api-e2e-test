Feature: Confirm booking
    

  Scenario: Confirm booking Succes
   # Given user athenticated and has role XXXXXXXXXXXXXXXXXX
    When I confirm booking with resource '/api/booking' with parameters:
      | k | CDEFGHI734 |
      | cid | 69dmfpf31 |
      | prc | 666 |
      | cur | MAD |
      | sd | 22/09/2023 |
      | ed | 23/09/2023 |
      | dateFormat | dd/MM/yyyy |
      | nb | 1 |
      | rm | Resort Room |
    
    Then Http code is 200
    Then the response is not empty
    Then the response is bizare
    
    
      Scenario: Confirm booking Failure
   # Given user athenticated and has role XXXXXXXXXXXXXXXXXX
    When I confirm booking with resource '/api/booking' with no parameters:
      | k |  |
      | cid |  |
      | prc |  |
      | cur |  |
      | sd |  |
      | ed |  |
      | dateFormat |  |
      | nb |   |
      | rm |  |
    
    Then Http code is 200
    Then the response is empty
   
    